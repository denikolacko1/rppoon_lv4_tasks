﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV4
{
    class Adapter
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> newData = dataset.GetData();
            int numberOfRows = newData.Count;
            int numberOfColumns = newData[0].Count;
            double[][] newMatrix = new double[numberOfRows][];
            for (int i = 0; i < numberOfRows; i++)
            {
                newMatrix[i] = new double[numberOfColumns];
            }
            for (int i = 0; i < numberOfRows; i++)
            {
                for (int j = 0; j < numberOfColumns; j++)
                {
                    newMatrix[i][j] = newData[i][j];
                }

            }
            return newMatrix;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }

    }
