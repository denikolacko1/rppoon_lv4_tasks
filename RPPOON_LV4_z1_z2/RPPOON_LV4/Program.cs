﻿using System;

namespace RPPOON_LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset(@"D:\rppoon_lv\csv.txt");
            Analyzer3rdParty analyze = new Analyzer3rdParty();
            Adapter adapt = new Adapter(analyze);
            double[] averagePerColumn = adapt.CalculateAveragePerColumn(dataset);
            double[] averagePerRows = adapt.CalculateAveragePerRow(dataset);
            foreach(double column in averagePerColumn)
            {
                Console.WriteLine(column);
            }
            foreach (double row in averagePerRows)
            {
                Console.WriteLine(row+"");
            }

        }
    }
}
