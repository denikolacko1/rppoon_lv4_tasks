﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rppoon_lv4_z3
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();

    }
}
