﻿using System.Collections.Generic;
using System;


namespace rppoon_lv4_z3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> library = new List<IRentable>();
            RentingConsolePrinter printingDevice = new RentingConsolePrinter();
            Video film = new Video("Avengers");
            Book book = new Book("Game of Thrones");
            library.Add(film);
            library.Add(book);
            printingDevice.DisplayItems(library);
            Console.WriteLine("And the price of these items combine is:");
            printingDevice.PrintTotalPrice(library);
        }
    }
}
